window.jQuery = window.$ = require('./jquery.js');
var Perspective = require('./perspective.js');
var Upload = require('./upload.js');

window.addEventListener("load", function() {
    // canvas要素
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext('2d');
    // 变形图片
    var canvas1 = document.createElement('canvas');
    canvas1.width = canvas.width;
    canvas1.height = canvas.height;
    var ctx1 = canvas1.getContext('2d');
    // 边框
    var canvas2 = document.createElement('canvas');
    canvas2.width = canvas.width;
    canvas2.height = canvas.height;
    var ctx2 = canvas2.getContext('2d');
    var op = null;
    var points = [[200, 200], [400, 200], [400, 280], [200, 280]];
    // img要素
    var img = new Image();
    img.crossOrigin = '';
    img.src = document.getElementById('cover').src;
    img.onload = function() {
        op = new Perspective(ctx1, img);
        op.draw(points);
        prepare_lines(ctx2, points);
        draw_canvas(ctx, ctx1, ctx2);
    };
    var drag = null;
    var diff = [], pOld, canMove;
    canvas.addEventListener("mousedown", function(event) {
        event.preventDefault();
        var p = get_mouse_position(event), i;
        pOld = p;
        if (ctx1.isPointInPath(p.x, p.y)) {
            for (i = 0; i < points.length; i++) {
                diff[i] = {
                    x: p.x - points[i][0],
                    y: p.y - points[i][1]
                };
            }
            canMove = true;
        }
        for( i=0; i<4; i++ ) {
            var x = points[i][0];
            var y = points[i][1];
            if( p.x < x + 10 && p.x > x - 10 && p.y < y + 10 && p.y > y - 10 ) {
                drag = i;
                canMove = false;
                break;
            }
        }
    }, false);
    canvas.addEventListener("mousemove", function(event) {
        event.preventDefault();
        var p = get_mouse_position(event);
        if (canMove && pOld) {
            for (var i = 0; i < points.length; i++) {
                points[i][0] = p.x - diff[i].x;
                points[i][1] = p.y - diff[i].y;
            }

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx1.clearRect(0, 0, canvas.width, canvas.height);
            op.draw(points);

            prepare_lines(ctx2, points);
            draw_canvas(ctx, ctx1, ctx2);
        }

        if(drag === null) {
            return;
        }
        points[drag][0] = p.x;
        points[drag][1] = p.y;
        prepare_lines(ctx2, points, true);
        draw_canvas(ctx, ctx1, ctx2);
    }, false);
    canvas.addEventListener("mouseup", function(event) {
        event.preventDefault();
        pOld = null;
        canMove = false;
        if(drag === null) {return; }
        var p = get_mouse_position(event);
        points[drag][0] = p.x;
        points[drag][1] = p.y;
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx1.clearRect(0, 0, canvas.width, canvas.height);
        var s = (new Date()).getTime();
        op.draw(points);

        prepare_lines(ctx2, points);
        draw_canvas(ctx, ctx1, ctx2);
        drag = null;
    }, false);
    canvas.addEventListener("mouseout", function(event) {
        event.preventDefault();
        drag = null;
        canMove = false;
    }, false);
    canvas.addEventListener("mouseenter", function(event) {
        event.preventDefault();
        drag = null;
        canMove = false;
    }, false);

    // 绑定UI事件
    var cover = document.querySelector('#canvas');
    var bg = document.querySelector('#bg');
    var saveBtn = document.querySelector('#save');
    var uploadBtn = document.querySelector('#upload');
    var loading = document.querySelector('.loading-box');
    var bgCtx = bg.getContext('2d');
    var outputCanvas = document.createElement('canvas');
    var outCtx = outputCanvas.getContext('2d');
    var canSave = false;
    var selectedImage;
    var size;
    
    outputCanvas.width = 640;
    outputCanvas.height = 420;

    document.querySelector('.pic-list').addEventListener('click', function(e) {
        selectedImage = e.target;
        if ( selectedImage.nodeName.toLowerCase() === 'img' ) {
            setBgCanvas(selectedImage);
        }
    });
    function setBgCanvas(img) {
        var naturalWidth = img.naturalWidth;
        var naturalHeight = img.naturalHeight;
        size = zoomToCanvas(naturalWidth, naturalHeight);

        bgCtx.clearRect(0, 0, 640, 420);
        bgCtx.drawImage(img, (640-size.width)/2, (420-size.height)/2, size.width, size.height);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canSave = true;

        uploadToAlpr(function (res) {
            var result = res.results[0];
            var coo;
            if (result && typeof result.coordinates !== 'undefined') {
                coo = result.coordinates;
                for (var i = 0; i < coo.length; i++) {
                    points[i][0] = coo[i].x;
                    points[i][1] = coo[i].y;
                }
            }

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx1.clearRect(0, 0, canvas.width, canvas.height);

            op.draw(points);

            prepare_lines(ctx2, points);
            draw_canvas(ctx, ctx1, ctx2);

            cover.style.display = 'block';
        });
    }
    function zoomToCanvas(w, h) {
        var width = w,
            height = h,
            p0 = 640/420,
            p = w/h;

        if (w > 640 || h > 420) {    
            if ( p < p0 ) {
                width = Math.min(w, 640);
                p = w/width;
                height = h/p;
            } else {
                height = Math.min(h, 420);
                p = h/height;
                width = w/p;
            }
        }

        return {
            width: width,
            height: height
        };
    }


    // drag drop
    bg.addEventListener('dragenter', function(e) {
        e.preventDefault();
    });
    bg.addEventListener('dragleave', function(e) {
        e.preventDefault();
    });
    bg.addEventListener('dragover', function(e) {
        ctx.clearRect(0,0, bg.width, bg.height);
        ctx.font = "60px Arial";
        bgCtx.fillStyle = '#FF0000';
        bgCtx.textAlign = 'center';
        bgCtx.fillText('快到碗里来～～', bg.width/2, bg.height/2);
        e.preventDefault();
    });
    document.body.addEventListener('dragenter', function(e) {
        e.preventDefault();
    });
    document.body.addEventListener('dragleave', function(e) {
        e.preventDefault();
    });
    document.body.addEventListener('dragover', function(e) {
        e.preventDefault();
    });

    function dropHandler(event) {
            event.preventDefault();
            var dt = event.dataTransfer;
            var fileList = dt.files;
            if (fileList.length === 0) {
                console.log('no file');
                return;
            }
            if (fileList[0].type.indexOf('image') === -1) {
                console.log('not image file');
                return;
            }

            var reader = new FileReader();
            var img = document.createElement('img');
            reader.onload = function(e) {
                img.onload = function () {
                    setBgCanvas(this);
                };
                img.src = this.result;
            };
            reader.readAsDataURL(fileList[0]);
        }
    bg.addEventListener('drop', dropHandler, false);
    document.body.addEventListener('drop', dropHandler, false);


    // save
    function buildImg() {
        if ( !canSave ) {
            return;
        }
        
        outCtx.clearRect(0,0, outputCanvas.width, outputCanvas.height);
        outputCanvas.width = size.width;
        outputCanvas.height = size.height;
        outCtx.drawImage(bg, -(640-size.width)/2, -(420-size.height)/2);
        outCtx.drawImage(canvas1, -(640-size.width)/2, -(420-size.height)/2);

        var url = outputCanvas.toDataURL("image/png");
        this.href = url;
    }
    saveBtn.addEventListener('click', buildImg);

    function uploadToAlpr(cb) {
        var data = bg.toDataURL("image/png");
        var file = dataURItoBlob(data);
        file.name = "tmp.png";
        file.type = "image/png";
        
        loading.style.display = 'block';
        // http://op.hushuibin.xqzuche.com/image/_upload?_of=json&waterMark=true
        var upload = new Upload({
            file: file,
            name:'boss-upload-file',
            url:'http://test.op.hushuibin.xqzuche.com:3000/alpr',
            data:{},
            progress:function(loaded,total){
                console.log(loaded/ total);
            },
            success:function(res){
                if (res) {
                    loading.style.display = 'none';
                    if (typeof cb === 'function') {
                        cb(res);
                    }
                }
            }
        });
        upload.upload();
    }

    var list = document.querySelector('.upload-list');
    // upload
    uploadBtn.addEventListener('click', function(e) {
        if ( !canSave ) {
            return false;
        }
        buildImg();
        var data = outputCanvas.toDataURL("image/png");
        var file = dataURItoBlob(data);
        file.name = "tmp.png";
        file.type = "image/png";
        
        loading.style.display = 'block';
        var upload = new Upload({
            file: file,
            name:'boss-upload-file',
            url: 'http://op.hushuibin.xqzuche.com/image/_upload?_of=json&waterMark=true',
            // url: 'http://test.op.hushuibin.xqzuche.com/upload',
            data:{},
            progress:function(loaded,total){
                console.log(loaded/ total);
            },
            success:function(res){
                if (res) {
                    loading.style.display = 'none';
                    // alert('上传完成');
                    var item = document.createElement('div');
                    item.className = 'item';
                    item.innerHTML = '<a target="_blank" href="'+ res.data.image_url +'">'+ res.data.image_url +'</a>';
                    list.appendChild(item);
                }
            }
        });
        upload.upload();
    });

}, false);

// 辅助方式
function prepare_lines(ctx, p, with_line) {
    ctx.save();
    //ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.clearRect(0, 0, 600, 450);
    if (typeof with_line === 'undefined') {
        with_line = true;
    }
    if( with_line === true ) {
        ctx.beginPath();
        ctx.moveTo(p[0][0], p[0][1]);
        for( var i=1; i<4; i++ ) {
            ctx.lineTo(p[i][0], p[i][1]);
        }
        ctx.closePath();
        ctx.strokeStyle = "red";
        ctx.stroke();
    }
    //
    ctx.fillStyle = "red";
    for( var i=0; i<4; i++ ) {
        ctx.beginPath();
        ctx.arc(p[i][0], p[i][1], 4, 0, Math.PI*2, true);
        ctx.fill();
    }
    //
    ctx.restore();
}

function draw_canvas(ctx, ctx1, ctx2) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    ctx.drawImage(ctx1.canvas, 0, 0);
    ctx.drawImage(ctx2.canvas, 0, 0);
}

function get_mouse_position(event) {
    var rect = event.target.getBoundingClientRect() ;
    return {
        x: event.clientX - rect.left,
        y: event.clientY - rect.top
    };
}


function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}
if (typeof module !== 'undefined') {
    module.exports = function(){};
}