var ToolBase = require('./ToolBase.js');
var img = new Image();
var toolbase;
var container = document.getElementById('container');

img.onload = function () {
	toolbase = new ToolBase({
		backgroundImage: img
	});

	var bgLayer = toolbase.layers[0];
	container.appendChild( bgLayer.canvas );

	bgLayer.moveTo(100, 10);
	bgLayer.scale(2, 2);
	// bgLayer.clip(0, 0, 200, 200);
	// bgLayer.scale(0.2, 0.2);
	window.tb = toolbase;
};
img.src = '/src/images/p1.jpg';

