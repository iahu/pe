var ToolBase = (function() {
    'use strict';
    var extend = require('./extend');
    var Layer = require('./Layer');

    function ToolBase(config) {
        // enforces new
        if (!(this instanceof ToolBase)) {
            return new ToolBase(config);
        }
        // constructor body
        this.config = {
            backgroundWidth: 640,
            backgroundHeight: 420
        };
        this.layers = [];
        extend(this.config, config);

        var bgi = config.backgroundImage, w, h;
        if (config && bgi) {
            w = Math.max(bgi.naturalWidth, bgi.width);
            h = Math.max(bgi.naturalHeight, bgi.height);

            if (w * h === 0) {
                throw Error('backgroundImage width and height can no be 0. (widht:'+ w + ', height:'+ h+')');
            }
            this.config.backgroundWidth = w;
            this.config.backgroundHeight = h;
        }
        this.init();
    }

    ToolBase.prototype = {
        init: function() {
            if ( !this.support.canvas ) {
                throw Error('canvas not supported.');
            }
            if (!this.support.dragDrop) {
                this.utils.log('drop and drop not supported.');
            }
            this.initBackgroundLayer();
            this.setCurrentLayer('background');
            this.utils.log('init');
        },
        initBackgroundLayer: function () {
            var config = this.config,
                w = config.backgroundWidth,
                h = config.backgroundHeight,
                bgi = config.backgroundImage,
                bgLayer = this.createLayer('background', w, h);
            if (bgi) {
                bgLayer.context.drawImage(bgi, 0, 0, w, h);
            }
            this.utils.log('initBackgroundLayer');
            return this;
        },
        createLayer: function (name) {
            var args = arguments, c, ctx;
            var conf = this.config;
            var w = conf.backgroundWidth;
            var h = conf.backgroundHeight;
            var l = new Layer(name, w, h);
            this.layers.push(l);
            return l;
        },
        getLayerByName: function(name) {
            var layers = this.layers;
            for (var layer in layers) {
                if (layers.hasOwnProperty(layer)) {
                    if ( layer.name === name ) {
                        return layer;
                    }
                }
            }
        },

        setCurrentLayer: function (n) {
            var l = this.getLayerByName(n);
            if ( l ) {
                this.currentLayer = l;
            }
        },

        buildOutput: function (format) {
            
        },

        support: {
            canvas: (function() {
                var c = document.createElement('canvas');
                return typeof c.getContext !== 'undefined' && c.getContext('2d');
            }()),

            dragDrop: (function () {
                var div = document.createElement('div');
                return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
            }())
        },

        utils: {
            dataURItoBlob: function (dataURI) {
                // convert base64/URLEncoded data component to raw binary data held in a string
                var byteString;
                if (dataURI.split(',')[0].indexOf('base64') >= 0)
                    byteString = atob(dataURI.split(',')[1]);
                else
                    byteString = unescape(dataURI.split(',')[1]);

                // separate out the mime component
                var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

                // write the bytes of the string to a typed array
                var ia = new Uint8Array(byteString.length);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }

                return new Blob([ia], {type:mimeString});
            },

            log: function () {
                if ( typeof console && console.log ) {
                    console.log.apply(console, arguments);
                }
            }
        }
    };
    return ToolBase;
}());
if (typeof module !== 'undefined') {
    module.exports = ToolBase;
} else {
    window.ToolBase = ToolBase;
}