// Layer constructor
var perspective = require('./perspective');
var extend = require('./extend');

function Layer (name, w, h) {
    if (!(name && w, h)) {
        throw Error('arguments error:', name, w, h);
    }
    var c = document.createElement('canvas');
    c.width = parseInt(w, 10);
    c.height = parseInt(h, 10);
    this.canvas = c;
    this.context = c.getContext('2d');
    this.name = name;
    this.x = 0;
    this.y = 0;
    this.originalWidth = w;
    this.originalHeight = h;
    this.image = null;
}
Layer.prototype = {
    constructor: Layer,
    putImage: function(img) {
        if (!img.width || !img.height) {
            return false;
        }
        this.image = img;
        this.context.draw(img, 0, 0);
        this.perspectiveObject = new perspective(this.context, img);
    },
    perspective: function(points) {
        po = this.perspectiveObject;
        if (!po) {
            return false;
        }
        po.draw(points);
    },
    clip: function (x,y,width,height) {
        var data = this.toImageData({
            x: x,
            y: y,
            width: width,
            height: height
        });
        this.clearContext();
        this.context.putImageData(data, x,y);
        return this;
    },
    scale: function(sx, sy) {
        var c = this.canvas;
        var oldW = c.width;
        var oldH = c.height;
        var w = oldW * sx;
        var h = oldH * sy;
        var data = this.toImageData();

        this.canvas.width = w;
        this.canvas.height = h;
        this.x = (oldW - w)/2;
        this.y = (oldH - h)/2;
        this.context.putImageData(data, 0, 0, 0, 0, w, h);
    },
    moveTo: function (x, y) {
        if (typeof x === 'undefined' || typeof y === 'undefined') {
            return false;
        }
        this.x = x;
        this.y = y;
        return this;
    },
    cut: function(x, y, width, height) {
        var data = this.toImageData({
            x: x,
            y: y,
            width: width,
            height: height
        });
        this.canvas.width = width;
        this.canvas.height = height;
        this.context.putImageData(data, 0, 0, ow, oh);
    },
    toImageData: function(option) {
        var c = this.canvas;
        var opt = extend({
            x: 0,
            y: 0,
            width: c.width,
            height: c.height
        }, option);
        
        return this.context.getImageData(opt.x,opt.y,opt.width, opt.height);
    },
    clearContext: function() {
        this.canvas.width = this.canvas.width;
    }
};

if (typeof module !== 'undefined' && typeof exports === 'object') {
    module.exports = Layer;
} else {
    this.Layer = Layer;
}