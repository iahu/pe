try {
    if (!XMLHttpRequest.prototype.sendAsBinary){
        XMLHttpRequest.prototype.sendAsBinary = function(datastr) {
            function byteValue(x) {
                return x.charCodeAt(0) & 0xff;
            }
            var ords = Array.prototype.map.call(datastr, byteValue);
            var ui8a = new Uint8Array(ords);
            this.send(ui8a.buffer);
        }
    }
} catch(e) {}
var Upload = function(params){
    this.file = params.file;
    this.success = params.success;
    this.progress = params.progress;
    this.data = params.data;
    this.name = params.name;
    this.url = params.url;
};
Upload.prototype = {
    upload:function(){
        var self = this;
        var reader = new FileReader();
        reader.onloadend = function(e){
            var xhr = new XMLHttpRequest();
            var boundary = '------multipartformboundary' + (new Date).getTime();
            var builder = self.builder(e.target.result,boundary);
            xhr.upload.addEventListener('progress',function(e){
                self.progress && self.progress(e.loaded, e.total);
            },false);
            xhr.onload = function(){
                if(xhr.responseText){
                    var res = null;
                    try{
                        res = JSON.parse(xhr.responseText);
                    }catch(e){
                        res = null;
                    }
                    self.success && self.success(res);
                }
            };
            xhr.open('POST',self.url,true);
            xhr.setRequestHeader('content-type', 'multipart/form-data; boundary=' + boundary);
            xhr.sendAsBinary(builder);
        };
        reader.readAsBinaryString(self.file);
    },
    builder:function(file_data,boundary){
        var dash = '--',
            crlf = '\r\n',
            builder = '';
        $.each(this.data, function(i, val) {
            if (typeof val === 'function') val = val();
            builder += dash;
            builder += boundary;
            builder += crlf;
            builder += 'Content-Disposition: form-data; name="'+i+'"';
            builder += crlf;
            builder += crlf;
            builder += val;
            builder += crlf;
        });
        builder += dash;
        builder += boundary;
        builder += crlf;
        builder += 'Content-Disposition: form-data; name="'+this.name+'"';
        builder += '; filename="' + this.file.name + '"';
        builder += crlf;

        builder += 'Content-Type: application/octet-stream';
        builder += crlf;
        builder += crlf;

        builder += file_data;
        builder += crlf;

        builder += dash;
        builder += boundary;
        builder += dash;
        builder += crlf;
        return builder;
    }
};
if (typeof module !== 'undefined') {
    if ( typeof exports !== 'undefined' ) {
        module.exports = Upload;
    }
}